﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleMovement : MonoBehaviour {

    public int m_PlayerNumber = 1;              // Int for input handling
    public float m_MaxSpeed = 2.0f;             // Max Speed of paddle

    private Rigidbody2D m_RigidBody2d;          // Rigidbody for the paddle to control movement
    private string m_MovementAxis;
    private float m_Velocity;

	// Use this for initialization
	void Start () {
        m_RigidBody2d = GetComponent<Rigidbody2D>();
        m_MovementAxis = "Vertical" + m_PlayerNumber;
	}
	
	// Update is called once per frame
	void Update () {
        m_RigidBody2d.velocity = new Vector2(0, 0);     // Set initial speed to 0 every update
        Movement();
	}

    // Handle paddle movement
    void Movement() {
        // Check if the player is trying to move the paddle
        if (Input.GetAxis(m_MovementAxis) != 0) {
            m_Velocity = Input.GetAxis(m_MovementAxis) * m_MaxSpeed;
            m_RigidBody2d.velocity = new Vector2(0, m_Velocity) * m_MaxSpeed;

        }
    }
}
